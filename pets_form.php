<!DOCTYPE html>
<html>
<head>
  <style>
    body {
      font-family: Arial, sans-serif;
      background-image: linear-gradient(rgba(0,0,50,0.8),rgba(0,0,50,0.8)), url(BG.jpg);
      background-size: cover;
    }
   
    .form-box{
    width: 60%;
    height: auto;
    max-width: 350px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background: #fff;
    padding: 50px 60px 70px;
    text-align: center;
    border-radius: 3%;
    }

    .form-box h1{
    font-size: 30px;
    color: #3c00a0;
    position: relative;
    }

    .form-box h1::after{
    content: '';
    border-radius: 3px;
    position: absolute;
    bottom: -12px;
    transform: translateX(-50%);
    }

    .input-field{
    background: #eaeaea;
    margin: 15px 0;
    border-radius: 3px;
    display: flex;
    align-items: center;
    max-height: 65%;
    transition: max-height 0.5s;
    overflow: hidden;
    }

    h1 {
      text-align: center;
    }

    #petForm {
      max-width: 300px;
      margin: auto;
      text-align: center;
    }

    #petForm label {
      display: block;
      margin-bottom: 10px;
    }

    #petForm input {
      width: 100%;
      padding: 10px;
    }

    #petForm button {
      padding: 10px 20px;
      background-color: #3c00a0;
      border: 10px;
      color: white;
      cursor: pointer;
    }

    #petForm button:hover {
      background-color: black;
    }
    .form-group {
    margin-bottom: 20px;
    }
  </style>
</head>
<body>
  <title>Add Pets</title>
  <div class="container">
    <div class="form-box"> 

  <h1>Add Pets</h1>
  <form id="petForm" action="upload.php" method="post">
    <div class="form-group">
    <input type="text" id="name" placeholder="Name:" name="name" required>
    </div>

    <div class="form-group">
    <input type="text" id="type" placeholder="Breed:" name="breed" required>
    </div>

    <div class="form-group">
    <input type="number" id="age" placeholder="Age:" name="age" required>
    </div>

    <div class="form-group">
    <input type="number" id="contact" placeholder="contact:" name="contact_info" required>
    </div>

    <div class="form-group">
    <input type="text" id="description" placeholder="Description:" name="description" required>
    </div>

    <div class="form-group">
    <input type="file" id="upload image" placeholder="Upload Image" name="image" required>
    </div>


    <button type="submit">Submit</button>
  </form>
  </div>
  </div>

  <script>
    document.getElementById('petForm').addEventListener('submit', function(event) {
      event.preventDefault();
      const formData = new FormData(event.target);

      fetch(event.target.action, {
        method: 'POST',
        body: formData
      })
      .then(response => response.text())
      .then(data => {
        alert('Pet added successfully: ' + data);  // Display response as an alert
        console.log(data); // Log response to the console
      })
      .catch(error => console.error('Error:', error));
    });
  </script>
</body>
</html>