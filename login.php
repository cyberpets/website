        <IDOCTYPE html>
            <html>
        <head>
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title> Sign In </title>
                <link rel="stylesheet" href="style.css">
                <script src="https://kit.fontawesome.com/efaae2519c.js" crossorigin="anonymous"></script>
        </head>
        <body>
        <div class="container">
            <div class="form-box">  
                <h1 id="title">Sign In</h1>
                <form action="process-login.php" method="post">
                    <div class="input-group">

                        <div class="input-field">
                            <i class="fa-solid fa-envelope"></i>
                            <input type="text" placeholder="Email" required  name= "email">
                        </div>

                        <div class="input-field">
                            <i class="fa-solid fa-lock"></i>
                            <input type="password" placeholder="Password" required name="password">
                        </div>
                        <p>Forget password?<a href="#"> Click Here!</a></p> 
                        <p>Don't have an account? <a href="signup.php">Sign Up</a></p>
                    </div>
                    <div class="btn-field">
                        <button type="submit" class="btn">Log in</button>
                        
                    </div>

                </form>
            </div>
        </div>


        </body>
        </html>